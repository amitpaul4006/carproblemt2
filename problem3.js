function Prob_fun(data_val) {
    if (data_val === undefined || data_val.length === 0) {
        return [];
    }
    let car_models = []
    for (let i = 0; i < data_val.length; i++) {

        car_models[i] = data_val[i]["car_model"].toLocaleLowerCase();
    }
    car_models.sort();
    return car_models;
}
module.exports = Prob_fun;