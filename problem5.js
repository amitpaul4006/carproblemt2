function Prob_fun5(data_val) {
    if (data_val === undefined || data_val.length === 0) {
        return [];
    }
    let year = []
    for (let i = 0; i < data_val.length; i++) {
        if (data_val[i]["car_year"] < 2000)
            year.push(data_val[i])
    }
    return year;
}
module.exports = Prob_fun5;