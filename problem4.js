function Prob_fun4(data_val) {
  if (data_val === undefined || data_val.length === 0) {
    return [];
  }
  let car_years = [];
  for (let i = 0; i < data_val.length; i++) {
    car_years[i] = data_val[i]["car_year"];
  }
  return car_years;
}

module.exports = Prob_fun4;