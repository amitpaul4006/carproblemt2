function Prob_fun(data_var, id) {
    if (data_var === undefined || data_var.length === 0) {
        return []; //data is empty.
    }
    if (id === undefined) {
        return []; //when id(n) not passed
    }
    else {
        for (let i = 0; i < data_var.length; i++) {
            if (data_var[i]["id"] === id) {
                return data_var[i];
            }
        }
        return []; //if n(id) is not in inventory or data

    }
}
module.exports = Prob_fun;