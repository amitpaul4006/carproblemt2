function Prob_fun6(data_val) {
    if (data_val === undefined || data_val.length === 0) {
        return [];
    }
    let bmw_audi = []
    for (let i = 0; i < data_val.length; i++) {
        if (data_val[i]["car_make"] == "Audi" || data_val[i]["car_make"] == "BMW") {
            bmw_audi.push(data_val[i])
        }

    }
    return JSON.stringify(bmw_audi);
}
module.exports = Prob_fun6;