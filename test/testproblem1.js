const data = require("../data");
const TestVal = require("../problem1");

let id = 33;
let data_var = data.inventory;
let result = TestVal(data_var, id);
if (result.length === 0) {
    console.log(result);
}
else {
    console.log(`Car ${id} is a ${result["car_make"]} ${result["car_model"]} ${result["car_year"]}`);
}
