const data = require('../data');
const test_data = require('../problem2');

let data_val = data.inventory;
const result = test_data(data_val);
if (result.length === 0) {
    console.log(result);
}
else {
    console.log(` Last car is a ${result["car_make"]}  ${result["car_model"]}`);
}
